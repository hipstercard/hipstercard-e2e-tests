![e2e-logo.png](src/main/resources/readme-images/e2e-logo.png)
# 🤖 E2E Tests
## 👋 **Welcome to the hipstercard e2e test repository!**
This repository was created specifically for the project's End-to-End (E2E) tests. 
Here, you can find comprehensive tests that check the functionality of [Cards Microservice](https://gitlab.com/hipstercard/hipstercard-card-service) and [Transactions Microservice](https://gitlab.com/hipstercard/hipstercard-transaction-service)

## 1. 🐰 Testing the RabbitMQ
- Since [Transactions Microservice](https://gitlab.com/hipstercard/hipstercard-transaction-service) communicates with the bank's processing center via RabbitMQ, I used RabbitTemplate in the tests to test reading and writing messages to this broker.
- The full code you can find here: [RabbitSenderAndReceiver.java](https://gitlab.com/hipstercard/hipstercard-e2e-tests/-/blob/main/src/test/java/hipstercard/dev/e2e/amqp/RabbitSenderAndReceiver.java) and [TransactionServiceTests.java](https://gitlab.com/hipstercard/hipstercard-e2e-tests/-/blob/main/src/test/java/hipstercard/dev/e2e/transaction/TransactionServiceTests.java), but there is code snippet of sending messages using RabbitTemplate and receiving them back and converting to List<TransactionDTO>:

![code-snippet](src/main/resources/readme-images/code-snippet.png)

## 🦊 2. Gitlab CI Configuration

![gitlab-ci-stages.png](src/main/resources/readme-images/gitlab-ci-stages.png)

- For now, I have `build_mr` stage that runs when merge request is created (its just builds the tests to check that code is valid)
- Also, I have `build` stage to check code that was merged to main
- And the most important stage is `test` stage with manual `test_all` job, that actually runs all the tests


