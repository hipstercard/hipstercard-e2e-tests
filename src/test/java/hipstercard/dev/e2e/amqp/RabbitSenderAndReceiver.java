package hipstercard.dev.e2e.amqp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import hipstercard.dev.e2e.config.RabbitMQTestConfiguration;
import hipstercard.dev.e2e.dto.TransactionDTO;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.time.Duration;
import java.util.List;

@Component
@ActiveProfiles("test")
@ContextConfiguration(classes = RabbitMQTestConfiguration.class)
public class RabbitSenderAndReceiver {
    private final ObjectMapper objectMapper;
    private final MessageProperties jsonMessageProperties;
    private final ParameterizedTypeReference<List<TransactionDTO>> transactionsTypeReference;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${spring.rabbitmq.exchanges.transaction-internal}")
    private String internalTransactionExchange;

    @Value("${spring.rabbitmq.routing-keys.internal-transaction}")
    private String internalTransactionRoutingKey;

    @Value("${spring.rabbitmq.queues.cashback-transaction}")
    private String cashbackTransactionQueue;

    public RabbitSenderAndReceiver() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        jsonMessageProperties = new MessageProperties();
        jsonMessageProperties.setContentType("application/json");

        transactionsTypeReference = new ParameterizedTypeReference<>() {
        };
    }

    public List<TransactionDTO> handleTransactions(List<TransactionDTO> transactions) throws JsonProcessingException {
        amqpTemplate.convertAndSend(internalTransactionExchange, internalTransactionRoutingKey,
                MessageBuilder.withBody(objectMapper
                        .writeValueAsBytes(transactions))
                        .andProperties(jsonMessageProperties)
                        .build());

        return amqpTemplate.receiveAndConvert(cashbackTransactionQueue,
                Duration.ofSeconds(10).toMillis(),
                transactionsTypeReference);
    }
}
