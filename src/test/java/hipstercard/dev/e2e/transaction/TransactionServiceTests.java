package hipstercard.dev.e2e.transaction;

import com.fasterxml.jackson.core.JsonProcessingException;
import hipstercard.dev.e2e.amqp.RabbitSenderAndReceiver;
import hipstercard.dev.e2e.config.RabbitMQTestConfiguration;
import hipstercard.dev.e2e.dto.TransactionDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("test")
@ContextConfiguration(classes = RabbitMQTestConfiguration.class)
public class TransactionServiceTests {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    @Autowired
    private RabbitSenderAndReceiver rabbitSenderAndReceiver;

    @Test
    public void testCardCashback() throws JsonProcessingException {
        // HIPSTER_PLATINUM (2%), Yandex-Market (1%), TRAVEL (1%) -> 2%
        List<TransactionDTO> transactions = List.of(TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("150000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("20000.00")))
                .organizationCode("organization:e44bde32-b7d2-470d-849d-72dd040d70ae")
                .operationCategory("TRAVEL")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build());

        List<TransactionDTO> cashbackTransactions =
                rabbitSenderAndReceiver.handleTransactions(transactions);

        Assertions.assertNotNull(cashbackTransactions);
        Assertions.assertEquals(1, cashbackTransactions.size());
        Assertions.assertEquals("400.00", cashbackTransactions.get(0).getTransactionSum().toString());
    }

    @Test
    public void testOperationCategoryCashback() throws JsonProcessingException {
        // HIPSTER_PLATINUM (2%), Lenta (4.5%), BOOKS (5.5%) -> 5.5%
        List<TransactionDTO> transactions = List.of(TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("150000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("10000.00")))
                .organizationCode("organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4")
                .operationCategory("BOOKS")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build());

        List<TransactionDTO> cashbackTransactions =
                rabbitSenderAndReceiver.handleTransactions(transactions);

        Assertions.assertNotNull(cashbackTransactions);
        Assertions.assertEquals(1, cashbackTransactions.size());
        Assertions.assertEquals("550.00", cashbackTransactions.get(0).getTransactionSum().toString());
    }

    @Test
    public void testOrganizationCashback() throws JsonProcessingException {
        // HIPSTER_PLATINUM (2%), Lenta (4.5%), VIDEOGAMES (2%) -> 4.5%
        List<TransactionDTO> transactions = List.of(TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("150000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("80000.00")))
                .organizationCode("organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4")
                .operationCategory("VIDEOGAMES")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build());

        List<TransactionDTO> cashbackTransactions =
                rabbitSenderAndReceiver.handleTransactions(transactions);

        Assertions.assertNotNull(cashbackTransactions);
        Assertions.assertEquals(1, cashbackTransactions.size());
        Assertions.assertEquals("3600.00", cashbackTransactions.get(0).getTransactionSum().toString());
    }

    @Test
    public void testOrganizationFlexibleCashback() throws JsonProcessingException {
        // HIPSTER_PLATINUM (2%), Ozon (~flexible~), TRAVEL (1%) -> ~flexible~
        List<TransactionDTO> transactions = List.of(TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("1500000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("10000.00")))
                .organizationCode("organization:9aac21a0-b3dc-4836-8aa4-537193745c10")
                .operationCategory("TRAVEL")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build(), TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("1500000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("50000.00")))
                .organizationCode("organization:9aac21a0-b3dc-4836-8aa4-537193745c10")
                .operationCategory("TRAVEL")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build(), TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("1500000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("150000.00")))
                .organizationCode("organization:9aac21a0-b3dc-4836-8aa4-537193745c10")
                .operationCategory("TRAVEL").
                date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build(), TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("15000000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("1500000.00")))
                .organizationCode("organization:9aac21a0-b3dc-4836-8aa4-537193745c10")
                .operationCategory("TRAVEL")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build());

        List<TransactionDTO> cashbackTransactions =
                rabbitSenderAndReceiver.handleTransactions(transactions);

        Assertions.assertNotNull(cashbackTransactions);
        Assertions.assertEquals(4, cashbackTransactions.size());
        Assertions.assertEquals("200.00", cashbackTransactions.get(0).getTransactionSum().toString());
        Assertions.assertEquals("2250.00", cashbackTransactions.get(1).getTransactionSum().toString());
        Assertions.assertEquals("7500.00", cashbackTransactions.get(2).getTransactionSum().toString());
        Assertions.assertEquals("75000.00", cashbackTransactions.get(3).getTransactionSum().toString());
    }

    @Test
    public void testNotEnoughBalanceBeforeTransaction() throws JsonProcessingException {
        // HIPSTER_PLATINUM (2%), Lenta (4.5%), VIDEOGAMES (2%) -> 4.5%
        List<TransactionDTO> transactions = List.of(TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("3000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("80000.00")))
                .organizationCode("organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4")
                .operationCategory("VIDEOGAMES")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build());

        List<TransactionDTO> cashbackTransactions =
                rabbitSenderAndReceiver.handleTransactions(transactions);

        Assertions.assertNotNull(cashbackTransactions);
        Assertions.assertEquals(1, cashbackTransactions.size());
        Assertions.assertEquals("0", cashbackTransactions.get(0).getTransactionSum().toString());
    }

    @Test
    public void testNotApprovedCashback() throws JsonProcessingException {
        // HIPSTER_PLATINUM (2%), Lenta (4.5%), MUSIC (50% - DECLINED) -> 0%
        List<TransactionDTO> transactions = List.of(TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("3000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("80000.00")))
                .organizationCode("organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4")
                .operationCategory("MUSIC")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build());

        List<TransactionDTO> cashbackTransactions =
                rabbitSenderAndReceiver.handleTransactions(transactions);

        Assertions.assertNotNull(cashbackTransactions);
        Assertions.assertEquals(1, cashbackTransactions.size());
        Assertions.assertEquals("0", cashbackTransactions.get(0).getTransactionSum().toString());
    }

    @Test
    public void testNotExistingCategory() throws JsonProcessingException {
        // HIPSTER_PLATINUM (2%), Lenta (4.5%), NOT_EXIST (0%) -> 0%
        List<TransactionDTO> transactions = List.of(TransactionDTO.builder()
                .bankAccountNumber("bank-account:6cc81406-ac5d-483a-b757-1755da3a309c")
                .cardId("card:b6e579d0-6765-4c5e-be1f-2fe60276e1ab")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("3000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("80000.00")))
                .organizationCode("organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4")
                .operationCategory("NOT_EXIST").date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build());

        List<TransactionDTO> cashbackTransactions =
                rabbitSenderAndReceiver.handleTransactions(transactions);

        Assertions.assertNotNull(cashbackTransactions);
        Assertions.assertEquals(1, cashbackTransactions.size());
        Assertions.assertEquals("0", cashbackTransactions.get(0).getTransactionSum().toString());
    }

    @Test
    public void testDefaultCardZeroCashback() throws JsonProcessingException {
        // HipsterDefault (0%), Lenta (4.5%), BOOKS (5%) -> 0%
        List<TransactionDTO> transactions = List.of(TransactionDTO.builder()
                .bankAccountNumber("bank-account:85763253-388f-4ab6-bf2f-82c1a1cc9f84")
                .cardId("card:e67444ca-bbc6-4b64-b5b0-3dc0072b1a0c")
                .balanceAfterTransaction(BigDecimal.valueOf(Double.parseDouble("3000.00")))
                .transactionSum(BigDecimal.valueOf(Double.parseDouble("80000.00")))
                .organizationCode("organization:qbe7eeb9-6fc4-497b-bb9b-4d7e1c199df4")
                .operationCategory("BOOKS")
                .date(ZonedDateTime.parse("2023-07-25T23:09:01.795+0200", formatter))
                .build());

        List<TransactionDTO> cashbackTransactions =
                rabbitSenderAndReceiver.handleTransactions(transactions);

        Assertions.assertNotNull(cashbackTransactions);
        Assertions.assertEquals(1, cashbackTransactions.size());
        Assertions.assertEquals("0", cashbackTransactions.get(0).getTransactionSum().toString());
    }
}
