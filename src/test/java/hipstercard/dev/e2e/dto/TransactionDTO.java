package hipstercard.dev.e2e.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDTO {
    @NotBlank(message = "Bank account number should not be blank!")
    @Pattern(regexp = "^bank-account:[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$",
            message = "Bank account number should be in format bank-account:{UUID}")
    private String bankAccountNumber;

    @NotBlank(message = "Card id should not be blank!")
    @Pattern(regexp = "^card:[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$",
            message = "Card id should be in format card:{UUID}")
    private String cardId;

    private BigDecimal balanceAfterTransaction;
    private BigDecimal transactionSum;

    @NotBlank(message = "Organization code should not be blank!")
    @Pattern(regexp = "^organization:[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$",
            message = "Organization code should be in format organization:{UUID}")
    private String organizationCode;

    @NotBlank(message = "Operation category should not be blank!")
    @Pattern(regexp = "^[A-Z]{4,}",
            message = "Operation category should consist only of latin uppercase letters")
    private String operationCategory;

    private ZonedDateTime date;
}
