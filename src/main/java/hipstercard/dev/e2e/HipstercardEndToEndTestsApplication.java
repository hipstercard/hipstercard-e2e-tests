package hipstercard.dev.e2e;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HipstercardEndToEndTestsApplication {
    public static void main(String[] args) {
        SpringApplication.run(HipstercardEndToEndTestsApplication.class, args);
    }
}
